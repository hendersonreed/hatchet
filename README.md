# Hatchet <img src="assets/hatchet.jpg" alt="hatchet logo" width="50"/>

An ultra-minimal static site generator, written in Haskell.

```
usage: hatchet [options] <input-dir> <output-dir>

  options:
    -h --help               displays this output
```

## Install

Clone the repo, cross your fingers and run "stack install"

## Basic Usage:

Pass a folder of Markdown to hatchet, along with the name of the output directory. Hatchet supports [Pandoc Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown) so some advanced features are or could be supported like inline LaTeX, footnotes, YAML metadata blocks, and more.

Hatchet has only a few requirements about the input directory passed to it:

* the directory must have a "header.html" and a "footer.html" file in it. These will be prepended and appended to each HTML fragment generated from the markdown source files.
* the directory also must contain a `posts` directory. The index page will only list posts from this directory.

## Features

Current Features:

* Basic functionality (convert markdown to webpages, add header and footer files.)
* Supports using CSS to style pages (in combination with the correct header and footer files.)
* Internal linking.
* Basic blog index page.
* inline LaTeX math-mode (through the use of WebTex, which embeds an svg of the generated LaTeX)

Feature Roadmap (not necessarily sorted in order of expected completion):

* RSS feed generation.


## Usage details

### Linking between pages.

If you want to link from one page to another, you can simply use basic markdown linking formatting, e.g. `[link text](link-url.html)`. All that's required to do internal linking between your pages is to write out the correct link for the generated html file. So if you want to link to a page titled `2020-02-02_Palindromes.md`, then in the URL portion of the link, put `2020-02-02_Palindromes.`**`html`**.

### Working with `header.html` and `footer.html`.

Really, there isn't much to say here. In the process of generating webpages, Hatchet converts your Markdown into an HTML page **fragment**. This means that it's not an entire page, but instead intended to be placed inside the `<body>` tag of a full page. The header and footer files provide the rest of that page, and will sandwich the HTML generated from your Markdown.

So your header should at least contain `<html><body>` and your footer should have `</body></html>`. But if you want more than that (a title bar, fancy links elsewhere, a page title, images, etc) then you'll need to put that in your header/footer files.

### Styling with CSS.

Hatchet doesn't really have "built-in" CSS handling, per se. All it will do is copy the CSS file you store alongside `header.html` and `footer.html` into the output directory. So it's important that you reference that CSS file in your header file if you want it to affect all your generated posts.

<!--

## Documentation and Architecture

To read the literate source in a nice format, you can use Pandoc to generate a pdf from the .lhs file. Run the following command:

```
pandoc  -o Hatchet.pdf src/Hatchet.lhs -f markdown+literate_haskell
```

-->

## Architecture

It's a fairly simplistic pipe and filter architecture, with the primary unit being markdown files in the `<input-dir>`.

![A diagram showing the operations done to files in the input-dir](assets/hatchet-architecture.png)
