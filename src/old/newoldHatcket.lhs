<!--
Hatchet - a minimal static site generator

Copyright (C) 2020 Henderson Hummel

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-->

Hatchet - a minimal site generator
==================================

> module Hatchet where

> import Control.Monad
> import Data.Maybe
> import Data.List
> import Data.Text as T hiding (any, concat, dropWhile, filter, head,
>                               isPrefixOf, isSuffixOf, map, zip)
> import System.Directory
> import System.FilePath
> import System.Posix.Files
> import Text.Pandoc

The base components of a static site generator are fairly simple. We need:

1. A way to copy files from one directory tree to another
2. A method of converting files from some format to HTML (we'll use Pandoc).
3. (optionally) some support for styling the generated pages.

Firstly, let's tackle how to copy files from one directory tree to another.

This is typically accomplished using a "tree walking" algorithm. It's a fairly
simple algorithm to implement in Haskell. Essentially, given a directory we
look through its contents. If an element of the directory is a file, we either
convert it or copy it over to the output directly. However, if it's a
directory, we simply call our recursive directory-walking function on it.

First however, we begin with a some types.

> type Conversion = (FilePath -> FilePath -> IO ())
> type Context = (FilePath, FilePath, Conversion)

A `Conversion` is a function that takes two filepaths, reads the contents of
the first, performs some conversion, and then write the converted text to the
second path.

A `Context` is simply a tuple of an input path, an output path, and a
Conversion function.

> convertDir :: Context -> IO ()
> convertDir (input, output, convertFunc) = do
>   inputContents <- listDirectory input
>   let inputPaths = map (input </>) inputContents
>   let outputPaths = map (output </>) inputContents
>   let contexts = [(x, y, z) | x <- inputPaths,
>                               y <- outputPaths,
>                               z <- [convertFunc]]
>   mapM_ handleContents contexts

> handleContents :: Context -> IO ()
> handleContents (inputPath, outputPath, convertFunc) = do
>   isDir <- isDirectory <$> getFileStatus inputPath
>   case isDir of
>       True  -> convertDir (inputPath, outputPath, convertFunc)
>       False -> copyOver (inputPath, outputPath, convertFunc)

> copyOver :: Context -> IO ()
> copyOver (inputPath, outputPath, convertFunc) = do
>   let outputDir = takeDirectory outputPath
>   createDirectoryIfMissing True outputDir -- the "True" ensures we create
>                                           -- parent directories if necessary.
>   let ext = takeExtension inputPath
>   case (ext == ".md") of
>       True  -> convertFunc inputPath outputPath
>       False -> customCopyFile inputPath outputPath

> customCopyFile :: FilePath -> FilePath -> IO ()
> customCopyFile inputPath outputPath = do
>   if shouldCopy inputPath
>       then copyFile inputPath outputPath
>       else pure ()

> shouldCopy :: FilePath -> Bool
> shouldCopy x =
>  (takeFileName x) /= "header.html"
>   || (takeFileName x) /= "footer.html" 

With the above functions, we can copy an entire directory from one location to
another. However, we haven't yet defined the conversion function that will be
used to convert our files from Markdown to HTML.

We will be using Pandoc, and converting each file to an HTML document fragment,
before we prepend and append the contents of the required `header.html` and
`footer.html` files.

As seen above, we need this conversion function to take two FilePaths and
perform an IO operation. The below function actually reads information in at
runtime and uses that information to create the conversion function.

> genConversionFunc :: FilePath -> IO (FilePath -> FilePath -> IO ())
> genConversionFunc baseDir = do
>   header <- readFile (baseDir </> "header.html")
>   footer <- readFile (baseDir </> "footer.html")
>   pure $ genHtml (sandwich header footer)

The `sandwich` function is curried to produce a function that automatically adds
the header and footer HTML to our converted file.

> sandwich header footer input = header ++ input ++ footer

The `genHtml` function is the only function that actually interacts with
Pandoc, and handles the reading of Markdown and the writing of HTML.
`readerOpts` and `writerOpts` are simply configuration options for Pandoc.

It takes a function that will modify the generated HTML, the path of a file to
read from, and the path to which to write the converted file.

> genHtml :: (String -> String) -> FilePath -> FilePath -> IO ()
> genHtml addHtml input output = do
>   fileContents <- readFile input
>   result <- runIO $ do
>     doc <- readMarkdown readerOpts (T.pack fileContents)
>     writeHtml5String writerOpts doc
>   html <- handleError result
>   let finalContent = addHtml (T.unpack html)
>   writeFile (replaceExtension output ".html") finalContent

> readerOpts :: ReaderOptions
> readerOpts = def {
>       readerExtensions = extensionsFromList [Ext_yaml_metadata_block,
>                                              Ext_tex_math_dollars]
> }

> writerOpts :: WriterOptions
> writerOpts = def {
>       writerHTMLMathMethod = WebTeX "https://latex.codecogs.com/svg.latex?"
>       , writerSectionDivs = True
>       , writerExtensions = extensionsFromList [Ext_tex_math_dollars]
> }

We have now reached a point where we have our conversion function, as well as
our function to walk directories. With those completed, we should be able to
combine the two to produce the final function that actually generates our site.

> genSite :: FilePath -> FilePath -> IO ()
> genSite inputDir outputDir = do
>   conversionFunc <- genConversionFunc inputDir
>   convertDir (inputDir, outputDir, conversionFunc)
