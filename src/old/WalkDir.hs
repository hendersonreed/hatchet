module WalkDir where

import Control.Monad
import Data.Maybe
import Data.List
import Data.Text as T hiding (any, concat, dropWhile, filter, head,
                              isPrefixOf, isSuffixOf, map, zip)
import System.Directory
import System.FilePath
import System.Posix.Files
import Text.Pandoc

convertDir :: FilePath -> FilePath -> IO ()
convertDir input output = do
  inputContents <- listDirectory input
  let inputPaths = map (input </>) inputContents
  let outputPaths = map (output </>) inputContents
  zipWithM_ handleContents inputPaths outputPaths

handleContents :: FilePath -> FilePath -> IO ()
handleContents inputPath outputPath = do
  isDir <- isDirectory <$> getFileStatus inputPath
  case isDir of
      True  -> convertDir inputPath outputPath
      False -> copyOver inputPath outputPath

copyOver :: FilePath -> FilePath -> IO ()
copyOver inputPath outputPath = do
  let outputDir = takeDirectory outputPath
  createDirectoryIfMissing True outputDir -- the "True" ensures we create
                                          -- parent directories if necessary.
  copyFile inputPath outputPath
