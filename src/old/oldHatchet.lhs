<!--
Hatchet - a minimal static site generator

Copyright (C) 2020 Henderson Hummel

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-->

Hatchet - a minimal site generator
=======

> module Hatchet where

> import Control.Monad
> import Data.Maybe
> import Data.List
> import Data.Text as T hiding (any, concat, dropWhile, filter, head,
>                               isPrefixOf, isSuffixOf, map, zip)
> import GHC.IO.Exception
> import System.Directory
> import System.FilePath
> import System.Posix.Files
> import Text.Pandoc

To begin with, the tool will simply offer one simplicistic level of usage:

```
  usage: hatchet <input directory> <name of output directory>

```

The input directory needs to contain at least header.html
and footer.html files, as well as a CSS file. If it doesn't, 
then hatchet will check in ~/.config/hatchet for those files. If there's no
header.html or footer.html, then hatchet will error out. A CSS file is not
required to convert a page.


basic process
-------------

So, this is essentially just a pipe and filter architecture, working on individual
files and going through the process of:

1. converting them from markdown to html
2. prepending and appending some html snippets to turn them into full pages and
3. writing the new files out to the output directory.

There is, importantly, the additional step of generating the index page, which will
link to all the generated pages.

Later, once all groundwork has been laid, we will tackle things like configuration via
file and more advanced commandline flags to configure things like css and how the root
of the site will be handled.


Prepending and appending some HTML
----------------------------------

Once we have some converted HTML, we need to wrap it in some more HTML, since
Pandoc doesn't produce full documents. We'll do that with the following
functions, which look for our header.html and footer.html functions and return
a function that will automatically wrap up any strings passed into it in the
header and footer html.

Here, we concatenate our three arguments together - this function is intended
to be curried before use with our html header and footer html.

> sandwich :: String -> String -> String -> String
> sandwich header foot input = header ++ "\n" ++ input ++ "\n" ++ foot

We can now write the function that will curry `sandwich` into a function that
will automatically add our header and footer html.

> loadHeadAndFoot :: [FilePath] -> IO (String -> String)
> loadHeadAndFoot filePaths = do
>   let headerFiles = filter (checkFileName "header.html") filePaths
>   configHeader <- getFromXdgConfig "header.html"
>   configFooter <- getFromXdgConfig "footer.html"

>   let headerFile = if (headerFiles == [])
>       then configHeader
>       else head headerFiles -- there shouldn't ever be multiple, but w/e
>   let footerFiles = filter (checkFileName "footer.html") filePaths
>   let footerFile = if footerFiles == []
>       then configFooter
>       else head footerFiles
>   header <- readFile headerFile
>   footer <- readFile footerFile
>   pure (sandwich header footer)

Simply a small utility function we'll use to check the filenames at the ends of
long FilePaths.

> checkFileName :: FilePath -> String -> Bool
> checkFileName name filePath = name == (takeFileName filePath)

We want to support getting a file from the users `~/.config/hatchet/`
directory. This function simply returns the path that file would be at, if it
existed.

> getFromXdgConfig :: FilePath -> IO FilePath
> getFromXdgConfig filename = do
>   confDir <- getXdgDirectory XdgConfig ""
>   pure (confDir ++ "/hatchet/" ++ filename)

Fixing up filenames
-------------------

To provide the name to write out our newly created HTML Pandoc object, we must
convert the file extension from `.md` to `.html`.

> changeName :: FilePath -> FilePath
> changeName x  =
>       if (takeExtension x == "")
>           then x
>           else replaceExtension x ".html"


Doing the conversion on a file
------------------------------

Ah, the function where all the work really happens. This function takes a filename
and loads the markdown from it, converting it using the Pandoc `readMarkdown`
monad as we go. Once we have it, we first add the header and footer html to it,
using the curried function we were passed. Then we can simply write it out to
another file in our destination directory. It makes heavy use of the Pandoc objects
and Reader/Writer classes, since that is where the conversion is actually happening.

Note that we are also configuring the readers and writers here. So, if in the
future we were going to read configuration files (foreshadowing...) this would
be the place we do it.

We also create a type named a `Context`. This is a tuple of 3 things: an input
path, an output path, and a function that prepends and appends the header and
footer html files.

> type Context = (FilePath, FilePath, (String -> String))

> genHtml :: Context -> IO ()
> genHtml (filename, outfileName, addHtml) = do
>   fileContents <- (readFile filename)
>   let readerOpts = def {
>       readerExtensions = extensionsFromList [Ext_yaml_metadata_block,
>                                              Ext_tex_math_dollars]
>   }
>   let writerOpts = def {
>       writerHTMLMathMethod = WebTeX "https://latex.codecogs.com/svg.latex?"
>       , writerSectionDivs = True
>       , writerExtensions = extensionsFromList [Ext_tex_math_dollars]
>   }
>   result <- runIO $ do
>     doc <- readMarkdown readerOpts (T.pack fileContents)
>     writeHtml5String writerOpts doc
>   html <- handleError result
>   let post = addHtml (T.unpack html)
>   writeFile (outfileName) post


Generating the Index Page
-------------------------

We are getting closer! Not quite there yet, we have some
things we want to add here - we need an index page. This can be created fairly
simply with the list of outfiles (just the names, actually, not the entire
path), the output directory, and the addHtml function.

we want to simply:
    1. transform the list of output names to some basic html links.
    2. call addHtml on that list of links
    3. write the new string out to a file named `index.html`.


This simply turns a filename into a more readable string.

I couldn't find this function in any packages, though it seems quite simple,
implementing the basic behavior of the classic Unix utility `tr`.

> tr :: String -> [Char] -> Char -> String
> tr str find replace =
>   map (\x -> if any (x ==) find then replace else x) str

> fileToTitle :: FilePath -> String
> fileToTitle x =
>   dropExtension (tr x "-_" ' ')


This simply adds some html around each string it's passed, turning it into a
basic HTML link. We add a CSS class, so it's easy to style these later on using
a custom css file or inline styling in our `header.html` if we want.

> turnToLink :: FilePath -> String
> turnToLink fileName =
>   "<a class=\"index-link\" href="
>       ++ fileName
>       ++ ">"
>       ++ (fileToTitle fileName)
>       ++ "</a><br>\n"


Finally, we put all that together in this function, which actually creates and
writes out an index file using the above functions.

> genIndex :: FilePath -> [FilePath] -> (String -> String) -> IO ()
> genIndex outputDir fileNames addHtml = do
>   let indexPages = filter (not . ("_" `isPrefixOf` )) fileNames
>   let links = concat $
>               (Data.List.reverse . sort) (map turnToLink indexPages)
>   let output = addHtml links
>   writeFile (outputDir ++ "/index.html") output


Managing any other files in the directory
-----------------------------------------

If our user has any other files they want to bundle with the site (custom css
files, images, javascript source) then we also need to ensure we copy those to
the newly created output directory.

Importantly, we need to *avoid* copying over header.html and footer.html.

> copyOthers :: FilePath -> FilePath -> [FilePath] -> IO ()
> copyOthers inputDir outputDir fileList = do
>   let hasHeadFooter = remove (isSuffixOf ".md") fileList
>   let hasFooter   = filter ("header.html" /=) hasHeadFooter
>   let finalFiles  = filter ("footer.html" /=) hasFooter
>   let inputFiles  = map (\x -> inputDir ++ "/" ++ x)  finalFiles
>   let outputFiles = map (\x -> outputDir ++ "/" ++ x) finalFiles
>   case finalFiles of
>       [] -> pure () -- empty list, aka we have no other files we need to copy.
>       (x:xs) -> zipWithM_ copyFile inputFiles outputFiles

> remove :: (a -> Bool) -> [a] -> [a]
> remove f = filter (not . f)

Because there are a few other things we might want to do before we get started
on the conversion itself, we'll package the above function with a bit more
functionality in a `prepOutputDir` function.

> prepOutputDir :: FilePath -> FilePath -> [FilePath] -> IO ()
> prepOutputDir inputDir outputDir fileList = do
>   _ <- createDirectoryIfMissing True outputDir -- create the output
>   -- directory, and all their parents if needed (that's what the True does).
>   _ <- copyOthers inputDir outputDir fileList -- copy non-md files over
>   if not $ any (isSuffixOf ".css") fileList -- no css file found
>       then copyCssFromConfigIfPossible outputDir
>       else pure ()

> copyCssFromConfigIfPossible outputDir = do
>   confDir <- getXdgDirectory XdgConfig ""
>   let hatchetCss = confDir ++ "/hatchet/styles.css"
>   cssExists <- doesPathExist hatchetCss
>   if cssExists
>       then copyFile hatchetCss (outputDir ++ "/styles.css")
>       else pure () -- can't do anything

checkFileName :: FilePath -> String -> Bool
checkFileName name filePath = name == (takeFileName filePath)

Putting it all together
-----------------------

Now, we have a function here that can actually accept both an input directory
and an output directory, and work through the given files.

> genSite :: FilePath -> FilePath -> IO ()
> genSite inputDir outputDir = do
>   fileList <- listDirectory inputDir
>   _ <- prepOutputDir inputDir outputDir fileList
>   let mdFiles = filter (isSuffixOf ".md") fileList

>   let allInputPaths = map (\x -> inputDir ++ "/" ++ x)  fileList
>   let inputPaths  = map (\x -> inputDir ++ "/" ++ x)  mdFiles
>   let outputPaths = map (\x -> outputDir ++ "/" ++ x)  (map changeName mdFiles)

>   addHtml <- loadHeadAndFoot allInputPaths
>   let genHtmlTuples = zip3 inputPaths outputPaths (repeat addHtml)
>   mapM_ genHtml genHtmlTuples
>   genIndex outputDir (map takeFileName outputPaths) addHtml



We want to recursively walk through a directory, performing one of several
actions on each element of a directory. If the element is another directory, we
want to call our recursive walking function on it. If the file is a markdown
file, we want to call our conversion function on it. Otherwise, we want to
simply copy it over to our output directory.

> walkDir :: Context -> IO ()
> walkDir context@(input, output, addhtml) = do
>   fileList <- listDirectory input
>   let tupleList = [(x, y) | x <- fileList, y <- [context]]
>   mapM_ handleContents tupleList

> handleContents :: (FilePath, Context) -> IO ()
> handleContents (filename, context@(input, output, addhtml)) = do
>   let ext = takeExtension filename 
>   isFile <- isRegularFile <$> getFileStatus (input ++ "/" ++ filename)
>   let newContext = (input ++ filename, output ++ filename, addhtml)
>   case isFile of
>       True -> case ext == ".md" of
>           True  -> genHtml ((input ++ "/" ++ filename), (output ++ "/" ++ filename), addhtml)
>           False -> copyFile (input ++ "/" ++ filename) (output ++ "/" ++ filename)
>       False -> walkDir newContext
