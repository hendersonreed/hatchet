module Conversion where

import Control.Monad
import Data.Maybe
import Data.List
import Data.Text as T hiding (any, concat, dropWhile, filter, head,
                              isPrefixOf, isSuffixOf, map, zip)
import System.Directory
import System.FilePath
import System.Posix.Files
import Text.Pandoc

genConvFunc :: FilePath -> IO (FilePath -> FilePath -> IO ())
genConvFunc baseDir = do
  header <- readFile (baseDir </> "header.html")
  footer <- readFile (baseDir </> "footer.html")
  pure $ genHtml (sandwich header footer)

sandwich header footer input = header ++ input ++ footer

genHtml :: (String -> String) -> FilePath -> FilePath -> IO ()
genHtml addHtml input output = do
  fileContents <- readFile input
  result <- runIO $ do
    doc <- readMarkdown readerOpts (T.pack fileContents)
    writeHtml5String writerOpts doc
  html <- handleError result
  let finalContent = addHtml (T.unpack html)
  writeFile (replaceExtension output ".html") finalContent

readerOpts :: ReaderOptions
readerOpts = def {
      readerExtensions = extensionsFromList [Ext_yaml_metadata_block
                                             ,Ext_tex_math_dollars
                                             ,Ext_header_attributes
                                             ,Ext_fenced_code_blocks
                                             ,Ext_backtick_code_blocks]
}

writerOpts :: WriterOptions
writerOpts = def {
      writerHTMLMathMethod = WebTeX "https://latex.codecogs.com/svg.latex?"
--      , writerSectionDivs = True
      , writerExtensions = extensionsFromList []
}

