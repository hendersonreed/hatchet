module Index where

import Control.Monad
import Data.Maybe
import Data.List
import Data.Text as T hiding (any, concat, dropWhile, filter, head,
                              isPrefixOf, isSuffixOf, map, zip)
import System.Directory
import System.FilePath
import System.Posix.Files
import Text.Pandoc

genIndex :: FilePath -> IO ()
genIndex outputDir = do
  files <- listDirectory (outputDir ++ "/posts/")
  let pages = filter isVisible files
  let markdownIndex = concat $ map turnToLink pages
  writeFile (outputDir </> "index.md") markdownIndex

isVisible :: FilePath -> Bool 
isVisible x =
  takeExtension x == ".html"
  && (head x) /= '_'
  && (takeFileName x) /= "index.html"

turnToLink :: String -> String
turnToLink x = 
  "[" ++ (fileToTitle x) ++ "]"  ++ "(" ++ "posts/" ++ x ++ ")\n\n"

tr :: String -> [Char] -> Char -> String
tr str find replace =
  map (\x -> if any (x ==) find then replace else x) str

fileToTitle :: FilePath -> String
fileToTitle x =
  dropExtension (tr x "-_" ' ')
