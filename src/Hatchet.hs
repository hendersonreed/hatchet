module Hatchet where

import Conversion
import Index


import Control.Monad
import Data.Maybe
import Data.List
import Data.Text as T hiding (any, concat, dropWhile, filter, head,
                              isPrefixOf, isSuffixOf, map, zip)
import System.Directory
import System.FilePath
import System.Posix.Files
import Text.Pandoc

type Conversion = (FilePath -> FilePath -> IO ())
type Context = (FilePath, FilePath, Conversion)

convertDir :: Context -> IO ()
convertDir (input, output, conv) = do 
  inputContents <- listDirectory input
  let inputPaths = map (input </>) inputContents
  let outputPaths = map (output </>) inputContents
  let contexts = zip3 inputPaths outputPaths (repeat conv)
  mapM_ handleContents contexts

handleContents :: Context -> IO ()
handleContents (inputPath, outputPath, conv) = do
  isDir <- isDirectory <$> getFileStatus inputPath
  case isDir of
      True  -> convertDir (inputPath, outputPath, conv)
      False -> copyOver (inputPath, outputPath, conv)

copyOver :: Context -> IO ()
copyOver (inputPath, outputPath, conv) = do
  let outputDir = takeDirectory outputPath
  createDirectoryIfMissing True outputDir -- the "True" ensures we create
                                          -- parent directories if necessary.
  let ext = takeExtension outputPath
  case (ext == ".md") of
    True  -> conv inputPath outputPath
    False -> customCopyFile inputPath outputPath

customCopyFile :: FilePath -> FilePath -> IO ()
customCopyFile src dest =
  if (shouldCopy $ takeFileName src)
    then copyFile src dest
    else pure ()

shouldCopy :: FilePath -> Bool
shouldCopy x =
  x /= "header.html"
  && x /= "footer.html"

genSite :: FilePath -> FilePath -> IO ()
genSite inputDir outputDir = do
  conv <- genConvFunc inputDir
  convertDir (inputDir, outputDir, conv)
  genIndex outputDir
  conv (outputDir </> "index.md") (outputDir </> "index.html")
  removeFile (outputDir </> "index.md")
  
