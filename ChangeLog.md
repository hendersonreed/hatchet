# Changelog for Hatchet

* implemented a directory-walking algorithm, allowing hatchet to work on arbitrarily tall directory structures.
* eliminated unit tests *gasp*
* removed support for header.html, footer.html, and CSS files in ~/.config/hatchet

## Unreleased changes
